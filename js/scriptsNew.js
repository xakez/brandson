d = document;
num = parseInt;
function tag(o, v) {
	return (v) ? (o.tagName.toLowerCase() == v.toLowerCase()) : o.tagName.toLowerCase();
}

function make_tabs(tabs, pads, opts) {


	var o1 = this, s = s || 0,

	def = {
		t:300,
		eff:function() {this.fadeIn(def.t)},
		click:function(e) {
			if (this.tagName.toLowerCase() == 'a') {
				e.preventDefault();
				e.stopPropagation();
			}

			var o = $(this), _p = this._p;

			if (def.hide && _p.s[0] == this) {
				o.removeClass('s');
				this.pad.hide();
				_p.s = false;
				return (false);
			}

			if (_p.s) {
				_p.s.removeClass('s');
				_p.s[0].pad.hide();
			}

			o.addClass('s');

			_p.s = o;
			def.eff.apply(this.pad)//this.pad.fadeIn(def.t);


		},
		hide:false
	};
	if (opts) $.extend(def, opts);

	if (def.hide) s = -1;

	tabs.each(function(i, o) {
		o.pad = $(pads[i]);
		o._p = o1;
		o.i = i;

		var o = $(o).click(def.click)

		if (o.hasClass('s') && !s) s = i;

	});


	o1.s = (def.hide) ? false : $(tabs[s]).addClass('s');
	o1.tabs = tabs;
	pads.filter(function(i) {return i != s}).hide();

	return o1;
}

function scroll1(o, opts) {
	var def = {items:3}, h = 0, index = 0;
	opts = $.extend(def, opts || {});

	o.itms = o.find('li');
	o.hold = o.find('ul');

	o.controls = $('<a href="#" class="next">Позднее</a><a href="#" class="prev">Ранее</a>').click(function() {
		(this.className == 'prev') ? index++ : index--;

		o.controls.filter(':hidden').fadeIn();
		if (!index) {
			o.find('.next').hide();
		}

		if (index > o.itms.length-opts.items-1) {
			o.find('.prev').hide();
		}

		o.hold.animate({'scrollTop': index*h}, 300, 'swing');  return (false)
	});

	o.hold.parent().append(o.controls);

	o.itms.each(function(i) {this.i = i; this.$ = o.itms.eq(i); if (this.className == 's') index = i;/*this.style.display = (i < opts.items) ? 'block' : 'none'; */if (i < opts.items) h+=this.$.outerHeight()});

	h = o.itms.height();

	o.hold.height(h*opts.items);

	$(function() {o.hold.scrollTop(index*h); if (!index) o.find('.next').hide();});

	return o;
}

function index_scroll(o, opts) {
	var def = {items:3}, h = 0, index = 0;
	opts = $.extend(def, opts || {});

	o.itms = o.find('li');
	o.hold = o.find('ul');

	o.controls = $('<a href="#" class="next">Позднее</a><a href="#" class="prev">Ранее</a>').click(function() {
		(this.className == 'prev') ? index++ : index--;

		o.controls.filter(':hidden').fadeIn();
		if (!index) {
			o.find('.next').hide();
		}

		if (index > o.itms.length/3-1) {
			o.find('.prev').hide();
		}

		o.hold.animate({'scrollTop': index*h}, 300, 'swing');  return (false)
	});

	o.hold.parent().append(o.controls);

	o.itms.each(function(i) {this.i = i; this.$ = o.itms.eq(i); if (this.className == 's') index = i;/*this.style.display = (i < opts.items) ? 'block' : 'none'; */if (i < opts.items) h+=this.$.outerHeight()});

	h = o.itms.height();

	//o.hold.height(h*opts.items);

	$(function() {o.hold.scrollTop(index*h); if (!index) o.find('.next').hide();});

	return o;
}

function make_promo() {
	var o = $('#promo'), arr = $('<div class="arr"/>').click(function() {
		var active_hold = this.parentNode.hold.eq(this.parentNode.index2);

		if (!active_hold.scrollLeft()) active_hold[0].index = 0;

		active_hold.scrollLeft(active_hold[0].index*745)
		active_hold[0].index++;

		//text.fadeOut();
		active_hold.animate({'scrollLeft':745*active_hold[0].index}, 500, 'swing', function() {
			//this.parentNode.show_text(active_hold.find('a').eq(active_hold[0].index));
			if (active_hold[0].index > 2) {active_hold[0].index = 0; active_hold.scrollLeft(0)};
		});
		return (false)
	}),
	text = $('<div class="text"/>'),
	i = 0;
	o.cars = o.find('.car');
	o.cars.each(function(i) {
		this.i = i; this.$ = o.cars.eq(i); this.hdr = this.$.find('.hdr');
		this.index2 = 0;
		this.hold = this.$.find('.hold').each(function(i) {this.index = 0; this.i = i; this.firstChild.innerHTML += this.firstChild.innerHTML});

		this.hold.not(':first').hide();

		this.hold.find('ul').width(745*6);
		this.itms = this.$.find('li').each(function(i) {this.i = i});

		this.n = this.itms.length-2;

		this.index = 0;

		this.show_text = function(o) {
			text.appendTo(this).html('<a href="'+o.attr('href')+'">'+(o.find('img').attr('alt') || 'Какой-то текст') + '</a> <img src="/i/push/arrr3.gif" align="absmiddle">').fadeIn(700);
		}

		})
		//.mouseover(function() {arr.appendTo(this) })
		.mousemove(function() {clearTimeout(this.t1);  return (false)})
		.mouseover(function() {this.hdr.hide(); clearTimeout(this.t1); return (false); })
		.mouseout(function(e) {var s = this; clearTimeout(s.t1); s.t1 = setTimeout(function() {
                //s.hdr.fadeIn(500, function() {s.flag = false;});
                s.hdr.fadeIn(0, function() {s.flag = false;});
                if (text[0].parentNode == s) text.hide(); if (!s.i) s.t2 = setTimeout(function() {$('#logo').show();}, 500)}, 300); });

	o.find('.hdr').mouseover(function(e) {
		clearTimeout(this.parentNode.t1);
		clearTimeout(this.parentNode.t2);
		var _p = this.parentNode;
		$(this).hide(); // $(this).fadeOut();

		_p.flag = true;
		if (!_p.i) $('#logo').hide();
		arr.appendTo(_p); text.css({opacity:'', display:'none'}); _p.show_text(_p.hold.eq(_p.index2).find('a').eq(_p.hold[_p.index2].index)); return (false)});

	o.parent().mousemove(function() {text.stop().attr('style','').hide();})



	$(function() {
		o.find('.hold').scrollLeft(0);
		o.t1 = setInterval(function() {
			var act, car;
			switch (i % 3) {case 0: act = 0; break; case 1: act = 2; break; case 2: act = 1};
			car  = o.cars.eq(act)[0];


			if (!car.flag) {
				if (++car.index2==car.hold.length) car.index2=0;
				car.hold.eq(car.index2).css('zIndex',10).fadeIn(900, function() {this.style.zIndex = 1; car.hold.eq((this.i) ? this.i-1 : car.hold.length-1).hide(); });
			}
			if (++i > 9) i = 1;
		}, 1000)
	})

	$('img.abkr').click(function() {o.find('.hdr').fadeIn(); return (false)})
}

function make_promoOLD() {
	var o = $('#promo'), arr = $('<div class="arr"/>').click(function() {
		var active_hold = this.parentNode.hold.eq(this.parentNode.index2);

		active_hold.scrollLeft(active_hold[0].index*745)
		active_hold[0].index++;

		text.fadeOut();
		active_hold.animate({'scrollLeft':745*active_hold[0].index}, 500, 'swing', function() {
			this.parentNode.show_text(active_hold.find('a').eq(active_hold[0].index));
			if (active_hold[0].index > 2) {active_hold[0].index = 0; active_hold.scrollLeft(0)};
		});
		return (false)
	}),
	text = $('<div class="text"/>'),
	i = 0;
	o.cars = o.find('.car');
	o.cars.each(function(i) {
		this.i = i; this.$ = o.cars.eq(i); this.hdr = this.$.find('.hdr');
		this.index2 = 0;
		this.hold = this.$.find('.hold').each(function(i) {this.index = i; this.innerHTML += this.innerHTML});

		this.hold.not(':first').hide();

		this.hold.find('ul').width(745*6);
		this.itms = this.$.find('li').each(function(i) {this.i = i});

		this.n = this.itms.length-2;

		this.index = 0;

		this.show_text = function(o) {
			text.appendTo(this).html('<a href="'+o.attr('href')+'">'+(o.find('img').attr('alt') || 'Какой-то текст') + '</a> <img src="/i/push/arrr3.gif" align="absmiddle">').fadeIn(700);
		}

		})
		//.mouseover(function() {arr.appendTo(this) })
		.mousemove(function() {clearTimeout(this.t1);  return (false)})
		.mouseover(function() {this.hdr.hide(); clearTimeout(this.t1); return (false); })
		.mouseout(function(e) {var s = this; clearTimeout(s.t1); s.t1 = setTimeout(function() {s.flag = false; s.hdr.fadeIn(function() {if (!s.i) $('#logo').show()}); if (text[0].parentNode == s) text.hide(); }, 300); });

	o.find('.hdr').mouseover(function(e) {
		var _p = this.parentNode;
		$(this).fadeOut();

		_p.flag = true;
		if (!_p.i) $('#logo').hide();
		arr.appendTo(_p); text.css({opacity:'', display:'none'}); _p.show_text(_p.hold.eq(_p.index2).find('a').eq(_p.hold[_p.index2].index)); return (false)});

	o.parent().mousemove(function() {text.stop().attr('style','').hide();})



	$(function() {
		o.find('.hold').scrollLeft(0);
		o.t1 = setInterval(function() {
			var act, car;
			switch (i % 3) {case 0: act = 0; break; case 1: act = 2; break; case 2: act = 1};
			car  = o.cars.eq(act)[0];


			if (!car.flag) {
				if (++car.index2==car.hold.length) car.index2=0;
				car.hold.eq(car.index2).css('zIndex',10).fadeIn(900, function() {this.style.zIndex = 1; car.hold.eq((this.index) ? this.index-1 : car.hold.length-1).hide(); });
			}
			if (++i > 9) i = 1;
		}, 1000)
	})
}

function scroll2(o, opts) {
	var def = {items:3}, h = 0, index = 0, w = 0, w2 = 0;
	opts = $.extend(def, opts || {});
	var index = 0;

	o.controls = $('<div class="arrr"/><div class="arrl"/>').click(function() {
		/*o.itms.css('opacity',0.5);*/
		(this.className == 'arrr') ? index++ : index--;
		if (index > o.itms.length/2) {index = 1; o.hold.scrollLeft(0)}
		if (index < 0) {index = o.itms.length/2-1;  o.hold.scrollLeft(o.scr.width()/2) } //o.hold.scrollLeft((o.itms.length/2)*w);

		o.hold.animate({'scrollLeft': o.itms[index].pos}, 300, 'swing'/*, function() {o.itms.eq(index).fadeTo(300, 1)}*/);  return (false)
	});

	o.scr = o.find('ul');
	o.itms = o.find('li')//.clone().appendTo(o.scr).parent().find('li');
	o.hold = o.scr.parent();

	o.itms.each(function(i) {if (this.className == 's') index = i; this.pos = w2; w2 += this.offsetWidth}).not(':nth-child('+(index+1)+')')//.css('opacity',0.5);

	w = o.itms.width();


	o.scr.width(w2);

	$(function() {
		o.hold.scrollLeft(0);
		if (o.scr.width()<= o.width()) {
			o.controls.hide();
		} else {
			o.itms = o.itms.clone().appendTo(o.scr).parent().find('li');
			o.itms[o.itms.length/2].pos = o.scr.width();
			o.scr.width(o.scr.width()*2)
		}})

	o.append(o.controls);
}

function scroll3(o, opts) {
	var def = {}, h = 0, index = 0, w = 0, dir = 1, step = 10, t = 3000, scrolled,  f = function() {scrolled = o.hold.scrollLeft(); if (!scrolled) {dir = 1} else if (scrolled >= o.scr.width()-o.hold.width()) {dir = 0}; o.t1 = setTimeout(function() {if (o[0].flag) return; (dir) ? o.hold[0].scrollLeft += num(step) : o.hold[0].scrollLeft -= num(step); f()}, 10);}

//	var def = {}, h = 0, index = 0, w = 0, dir = 1, step = 10, t = 3000, scrolled,  f = function() {scrolled = o.hold.scrollLeft(); if (!scrolled) {dir = 1} else if (scrolled >= o.scr.width()-o.hold.width()) {dir = 0}; o.t1 = setTimeout(function() {(dir) ? o.hold[0].scrollLeft += num(step) : o.hold[0].scrollLeft -= num(step); f()}, 10);}
	opts = $.extend(def, opts || {});
	var index = 0;
	o.hold = o.find('.hold');
	/*o.controls = $('<div class="arrr"/><div class="arrl"/>').click(function() {
		(this.className == 'arrr') ? ((o.hold.scrollLeft()+o.width() >= o.scr.width()) ? index : index++) : index--;
		if (index < 0) {index = 0}
		o.hold.animate({'scrollLeft': index*w}, 300, 'swing', function() {
			o.itms.eq(index).fadeTo(300, 1)
			o.controls.eq(0).css('display', (o.hold.scrollLeft() >= o.scr.width()-o.hold.width()) ? 'none' : 'block');
			o.controls.eq(1).css('display', (o.hold.scrollLeft()) ? 'block' : 'none')
		});  return (false)
	});*/

	o.scr = o.find('ul');
	o.itms = o.find('li');

	w = o.itms.width();

	step = w/t*10;

	o.scr.width(o.itms.length*w);
        o.hover(function() {this.flag = true; return (false);}, function() {this.flag = false; f(); return (false);})

	$(function() {o.hold.scrollLeft(index*w); /*if (!o.hold.scrollLeft()) o.controls.eq(1).hide();*/ if (o.scr.width()>o.width()) f();})



	/*o.append(o.controls);*/
}

function portfolio(o, opts) {
	opts = $.extend({}, opts);

	o.itms = o.find('li').each(function() {var img = this.getElementsByTagName('img')[0]; img.style.background = 'url('+img.src+')'; img.src = 'i/t.gif', img.className = 't'});
	o.filter = opts.filter.click(function() {
	this.className = (this.className.length) ? '' : 's';

	var filter = [];
	o.filter.each(function() {if (this.className == 's') filter.push('.'+this.href.substr(this.href.indexOf('#')+1))});
	o.itms.hide().filter(filter.join(',')).fadeIn();
	/*if (this.className != 's') {this.className = 's'; o.find('.'+this.href.substr(this.href.indexOf('#')+1)).fadeIn()} else {this.className = ''; o.find('.'+this.href.substr(this.href.indexOf('#')+1)).fadeOut();};*/

	return (false)})

}


function scroll3n(o, opts) {
	var def = {}, h = 0, index = 0, w = 0;
	opts = $.extend(def, opts || {});
	var index = 0;
	o.hold = o.find('.hold');
	o.controls = $('<div class="arrr"/><div class="arrl"/>').click(function() {
		(this.className == 'arrr') ? index++ : index--;
		if (index > o.itms.length/2) {index = 1; o.hold.scrollLeft(0)}
		if (index < 0) {index = o.itms.length/2-1;  o.hold.scrollLeft(o.scr.width()/2) }
		o.scr.parent().animate({'scrollLeft': index*w}, 300, 'swing', function() {o.itms.eq(index).fadeTo(300, 1)});  return (false)
	});

	o.scr = o.find('ul');
	o.itms = o.find('li');

	w = o.itms.width();

	o.scr.width(o.itms.length*w);

	$(function() {
		o.scr.parent().scrollLeft(index*w)
		if (o.scr.width()<= o.width()) {
			o.controls.hide();
		} else {
			o.itms = o.itms.clone().appendTo(o.scr).parent().find('li');
			o.itms[o.itms.length/2].pos = o.scr.width();
			o.scr.width(o.scr.width()*2)
		}
	})

	o.append(o.controls);
}