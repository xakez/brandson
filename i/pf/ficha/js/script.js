function win_resize() {
   var content_left = $('.content_left').width();
   var p_width = 455;
   $('.content_left h2,.content_left p').css('margin-left', ((content_left-p_width)/2) + 'px');
   var task_left_width = $('#task_left').width();
   var formula = 230 - (480 - task_left_width);
   if (formula > 185) {
      $('#task_center_arrow').css('margin-bottom', formula + 'px');
   } else {
      $('#task_center_arrow').css('margin-bottom', 185 + 'px');
   }
   var window_width = $(window).width();
   if (window_width < 1400) {
      $('#task_text_left').appendTo($('#append_text'));
      $('#task_text_right').appendTo($('#append_text'));
      $('#wrapper_task').css('height', 1548 + 'px');
   } else {
      $('#task_text_left').appendTo($('#append_text_again'));
      $('#task_text_right').appendTo($('#append_text_again'));
      $('#aim_audiotory').appendTo($('#append_text_again'));
      $('#wrapper_task').css('height', 1370 + 'px');
   }
   console.log('123');
}

$(document).ready(function() {
     $('#project_menu a[href^="#"]').click(function(){
        var target = $(this).attr('href');
        $('html, body').animate({scrollTop: $(target).offset().top-100}, 500);
        return false; 
   }); 
   win_resize();
	$(window).resize(function() {
      win_resize();
   });

   $('#pageflip').hover(function() {
       $('#pageflip img, .peel_block').stop().animate({
       width: '370px',
       height: '200px',
       },500);
       }, function() {
       $('#pageflip img').stop().animate({
       width: '300px',
       height: '162px',
       }, 220);
       $('.peel_block').stop().animate({
       width: '300px',
       height: '162px',
       }, 200);
   });
   $('#pageflip1').hover(function() {
       $('#pageflip1 img, .peel_block1').stop().animate({
       width: '370px',
       height: '200px',
       },500);
       }, function() {
       $('#pageflip1 img').stop().animate({
       width: '300px',
       height: '162px',
       }, 220);
       $('.peel_block1').stop().animate({
       width: '300px',
       height: '162px',
       }, 200);
   });
});

