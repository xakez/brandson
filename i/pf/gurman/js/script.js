$(document).ready(function() {
	document.onmousemove = mouseMove;
	startX = -1;
	function fixEvent(e) {
         // получить объект событие для IE
         e = e || window.event

         // добавить pageX/pageY для IE
         if ( e.pageX == null && e.clientX != null ) {
            var html = document.documentElement
            var body = document.body
            e.pageX = e.clientX + (html && html.scrollLeft || body && body.scrollLeft || 0) - (html.clientLeft || 0)
            e.pageY = e.clientY + (html && html.scrollTop || body && body.scrollTop || 0) - (html.clientTop || 0)
         }

         // добавить which для IE
         if (!e.which && e.button) {
            e.which = e.button & 1 ? 1 : ( e.button & 2 ? 3 : ( e.button & 4 ? 2 : 0 ) )
         }

         return e
    }

      function mouseMove(event){
         event = fixEvent(event);
         newX = event.pageX;
         if (startX === -1)
            startX === newX;

         var nb = $('#new_icon'),
            wid = nb.width(),
            offsetX = nb.offset().left,
            sd = offsetX-newX;

         wid = wid+sd;
         if (wid<0)
            wid = 0;
         if (wid>617)
            wid=617;
         nb.css("width", wid+"px");


      };

      $(window).resize(function() {
         $('.rslides li').each(function() {
            var li_width = $(this).width();
            console.log(li_width);
            var img_width = $('img',this).width();
            console.log(img_width);
            if (li_width < img_width) {
               var ml = (Math.abs(li_width - img_width)) / 2;
               $('img',this).css('margin-left', -ml + 'px');
               $('div', this).css('margin-left', ml + 'px');
            } else {
               $('img',this).css('margin', '0 auto');
               $('div', this).css('margin', '0 auto');
            }
         });
      });
});